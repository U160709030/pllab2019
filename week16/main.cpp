#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <chrono>

using namespace std;

struct LinkedList {
   string data;
   struct LinkedList *next;
};

struct LinkedList* head = NULL;

void save(string new_data) {
   struct LinkedList* new_node = new LinkedList;
   new_node->data = new_data;
   new_node->next = head;
   head = new_node;
}

bool compare(string name) {
   struct LinkedList* ptr;
   ptr = head;
   while (ptr != NULL) {
      if(name == ptr->data){
        return true;
      };
      ptr = ptr->next;
   }
   return false;
}

int main()
{
    auto start = chrono::steady_clock::now( );
    ifstream inp1;
    map<string, int> list1;
    string line;
    string file1;
    string file2;
    cout<<"enter the first file's path"<<endl;
    cin>>file1;
    
    cout<<"enter the second file's path"<<endl;
    cin>>file2;
    ifstream inp2;

    inp1.open(file1);
    for(int i = 1; getline(inp1, line); i++){
        list1[line] = i;
    }
    inp1.close();

    inp1.open(file2);
    while(getline(inp1, line)){
        save(line);
    }
    inp1.close();

    for(map<string, int>::iterator it = list1.begin(); it != list1.end(); ++it){
        string key = it->first;
        int value  = it->second ;
        if(!(compare(key))){
            cout << key << " in first file at line " << value << " is missing in second file!" << endl;
        }
    }
    auto elapsed = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now( ) - start );
    cout << elapsed.count() << " ms" << endl;


    return 0;
}
