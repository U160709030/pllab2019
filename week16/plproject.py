import time
import sys
# ########################################################################################################################
# # first file's hashMap implementation

file1 = open(sys.argv[1], "r")




f1_dict = dict()

f1LineNum = 1
for line in file1:
    line_stripped = line.rstrip("\n")
    f1keys = f1_dict.keys()
    # if you want to include the empty strings into your search, uncomment the line below
    # if line_stripped != '':
    if line_stripped in f1keys:
        f1_dict[line_stripped].append(f1LineNum)
    else:
        f1_dict[line_stripped] = [f1LineNum]
    f1LineNum += 1




# #######################################################################################################################+

# second file's linkedList implementation
f2_list = []


class Node:
    def __init__(self, val):
        self.val = val
        self.next = None
        f2_list.append(self.val)


file2 = open(sys.argv[2], "r")


f2LineNum = 1
for line in file2:
    line_stripped = line.rstrip("\n")
    nodeName = 'node' + str(f2LineNum)
    nodeName = Node(line_stripped)
    f2LineNum += 1
# ######################################################################################################################


# searching

checkvar=0
search_start = time.time()
for key in f1_dict:
    if key not in f2_list:
        print("first line's string", key, "in line(s)", f1_dict[key], "is missing in second file\n")
        checkvar += 1
if checkvar == 0:
    print("two files contain same strings")
search_end = time.time()

print("Searching two files took", (search_end-search_start), "seconds")